import queue
import unittest
from attendance.classroom.classroom import ClassRoom
from attendance.classroom.student import Student
from attendance.runner import Runner
import cv2
from deepface import DeepFace
import base64
import numpy as np

class TestRunner(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_images(self):
        expected = [
            './tests/data_set/class_room/1/aluno_1.png',
            './tests/data_set/class_room/1/aluno_2.png',
            './tests/data_set/class_room/1/aluno_3.png',
            './tests/data_set/class_room/1/aluno_4.png',
            './tests/data_set/class_room/1/aluno_5.png',
            './tests/data_set/class_room/1/daniel_1.png',
            './tests/data_set/class_room/1/pedro_1.png',
        ]

        result = Runner.get_images("./tests/data_set/class_room/1")
        self.assertEqual(result, expected)

    def test_image_compare(self):
        print('Running test image compare')

        first_image = './tests/data_set/class_room/1/aluno_2.png'
        second_image = './tests/data_set/camera_presence/aluno_2_1.png'
        result = Runner.compare_images(first_image, second_image)
        print("Aluno 2", result)
        assert result['verified'] == True

        first_image = './tests/data_set/class_room/1/aluno_2.png'
        second_image = './tests/data_set/camera_presence/pedro_1.png'
        result = Runner.compare_images(first_image, second_image)
        print("Aluno 2", result)
        assert result['verified'] == False

        first_image = './tests/data_set/class_room/1/aluno_4.png'
        second_image = './tests/data_set/camera_presence/aluno_4_2.png'
        result = Runner.compare_images(first_image, second_image)
        print("Aluno 4", result)
        assert result['verified'] == True

        first_image = './tests/data_set/class_room/1/daniel_1.png'
        second_image = './tests/data_set/camera_presence/daniel_1.png'
        result = Runner.compare_images(first_image, second_image)
        print("Pedro", result)
        assert result == False

    def test_check_attendance(self):
        print('Running test check attendance')

        pedro = Student(1, 'Pedro', ['pedro_1.png'])
        daniel = Student(2, 'Daniel', ['daniel_1.png'])
        aluno_1 = Student(3, 'Aluno 1', ['aluno_1.png'])
        aluno_2 = Student(4, 'Aluno 2', ['aluno_2.png'])
        aluno_3 = Student(5, 'Aluno 3', ['aluno_3.png'])
        aluno_4 = Student(6, 'Aluno 4', ['aluno_4.png'])
        aluno_5 = Student(7, 'Aluno 5', ['aluno_5.png'])

        classroom = ClassRoom(1, [pedro, daniel, aluno_1, aluno_2, aluno_3, aluno_4, aluno_5])

        mqtt_messages = queue.Queue()
        runner = Runner(mqtt_queue=mqtt_messages, classroom=classroom)

        images = Runner.get_images('./tests/data_set/camera_presence/')
        for image in images:

            img_vector = cv2.imread(image)
            student = runner.check_attendance(img_vector)
            if student:
                print("Matched: " + image.split('/')[-1] + " - " + student.name)
            else:
                print("Not matched: " + image.split('/')[-1])

    def test_runner(self):
        print('test2')
        classroom = ClassRoom(1, "/workspace/tests/data_set/class_room/1")

        mqtt_messages = queue.Queue()
        runner = Runner(mqtt_queue=mqtt_messages, classroom=classroom)

        # Add to queue
        # runner.worker()


def main():
    unittest.main()

if __name__ == '__main__':
    main()
