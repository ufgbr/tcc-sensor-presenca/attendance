FROM python:3.8

RUN apt update

RUN apt install -y \
    ffmpeg \
    libsm6 \
    libxext6 \
    build-essential \
    cmake \
    pkg-config

RUN python3 -m pip config --user set global.timeout 350

RUN pip install poetry

RUN poetry config virtualenvs.create false

COPY . ./

RUN poetry install
