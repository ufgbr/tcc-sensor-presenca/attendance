import queue
# from multiprocessing import Process, Queue
import paho.mqtt.client as mqtt
import json
import threading
from flask import Flask, Response
from .runner import Runner

app = Flask(__name__)

def on_message(mqtt_client, userdata, msg):
    try:
        payload = json.loads(msg.payload)

        print("on_message: ")
    except:
        print('error deconding json')
        return

    topic = msg.topic.split('/')
    # print(topic)

    mqtt_messages['camera_1'].put(payload)


@app.route('/')
def video_feed():
    return 'hello world'

def mqtt_thread():
    client = mqtt.Client()

    client.connect("mqtt-server", 1883, 60)
    client.on_message = on_message
    client.subscribe("camera_presence/camera_1/feed")

    client.loop_forever()

def main():
    print('Main on thread: ', threading.get_ident())

    global mqtt_messages
    mqtt_messages = {'camera_1': queue.Queue()}

    t1 = threading.Thread(target=mqtt_thread, daemon=True)
    t1.start()

    runner = Runner(mqtt_messages['camera_1'])
    runner.run()

    #DO !!!!!!!NOT!!!!!!! RUN WITH DEBUG, DOUBLE OF THREADS

    app.run(host='localhost', debug=False)
    # client.loop_forever()

if __name__ == '__main__':
    main()
