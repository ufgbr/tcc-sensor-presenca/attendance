from asyncio import Queue
import os
import re
import time
import json
# from multiprocessing import Process
import threading
import cv2
import numpy as np
from threading import Thread
from deepface import DeepFace

import base64

from attendance.classroom.classroom import ClassRoom

class Runner:
    def __init__(self, mqtt_queue: Queue, classroom: ClassRoom):
        self.mqtt_queue = mqtt_queue
        self.classroom = classroom
        
    @staticmethod
    def decode_base64_image(image: str):
        #KILL MEEE
        debug = False
        print('Got on base64 decode')
        # print('Publishing to Mqtt Cropped shape')
        # print(image.shape)
        image_jpg = base64.b64decode(image)


        #TODO test with frombuffer only
        img_vector = cv2.imdecode(np.frombuffer(image_jpg, np.uint8), cv2.IMREAD_COLOR)

        # print(img_vector)
        
        if debug: 
            cv2.imwrite('/workspace/output_images/' + str(time.time()) + '.jpg', img_vector)

        return img_vector

    @staticmethod
    def get_images(path):
        dirs = os.listdir(path)
        images = []
        for file in dirs:
            if file.endswith(".png") or file.endswith(".jpg"):
                images.append(path + "/" + file)

        return images

    @staticmethod
    def compare_images(face, reference):
        try:
            result = DeepFace.verify(face, reference, model_name='Facenet512', distance_metric='euclidean_l2')
            return result
        except Exception as e:
            pass
            # print("Could not compare images: ", e)

        return False

        
    def check_attendance(self, face):
        
        for student in self.classroom.students:
            for image in student.images:
                image = '/workspace/tests/data_set/class_room/1/' + image
                res = self.compare_images(face, image)
                if(res and res['verified'] == True):
                    return student
        
        return False

    def work(self):
        print("Got on runner: ")
        data = self.mqtt_queue.get()
        face = self.decode_base64_image(data['data']['image'])

        self.check_attendance(face)

    def worker(self):
        print('New class thread, id: ', threading.get_ident())
        while True:
            self.work()

    def run(self):
        t1 = Thread(target=self.worker, daemon=True)
        t1.start()
        # p1 = Process(target=self.worker)
        # p1.start()
